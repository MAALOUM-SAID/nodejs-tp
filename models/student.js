const Sequelize=require('sequelize');
const sequelize=require('../config/config');
const Student=sequelize.define('students',{
    id:{
        type:Sequelize.INTEGER,
        autoIncrement:true,
        primaryKey:true
    },
    nom:{
        type:Sequelize.STRING,
        allowNull:false,
    },
    prenom:{
        type:Sequelize.STRING,
        allowNull:false,
    },
    ville:{
        type:Sequelize.STRING,
        allowNull:false,
    },
    filiere:{
        type:Sequelize.STRING,
        allowNull:false,
    },
    total_absences:{
        type:Sequelize.INTEGER,
        allowNull:true,
    },
});
module.exports=Student;