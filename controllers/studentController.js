const Student=require('../models/student');
exports.getAllStudents=(req,res)=>{
    Student.findAll()
    .then(students=>{
        res.json(students);
    })
    .catch(err=>{
        req.status(500)
        .send('Une erreur est survenue lors de la récupération des étudiants');
    });
}
exports.getStudentById=(req,res)=>{
    Student.findByPK(id)
    .then(student=>{
        if (student) {
            res.json(student);
        }else{
            res.status(404).send("L'étudiant demandé est introuvable")
        }
    })
    .catch(err=>{
        req.status(500)
        .send('Une erreur est survenue lors de la récupération des étudiants');
    });
}
exports.addNewStudent=(req,res)=>{
    const {nom,prenom,ville,filiere}=req.body;
    Student.create({nom,prenom,ville,filiere})
    .then(student=>{
        res.json(student);
    })
    .catch(err=>{
        req.status(500)
        .send('Une erreur est survenue lors de la récupération des étudiants');
    });
}
exports.updateStudent=(req,res)=>{
    const id = req.params.id;
    const {nom,prenom,ville,filiere}=req.body;
    Student.findByPK(id)
    .then(student=>{
        if(student){
            student.nom=nom;
            student.prenom=prenom;
            student.ville=ville;
            student.filiere=filiere;
            return student.save();
        }else{
            req.status(404).send("L'étudiant demandé est introuvable")
        }
    })
    .catch(err=>{
        req.status(500)
        .send('Une erreur est survenue lors de la récupération des étudiants');
    });
}
exports.deleteStudent=(req,res)=>{
    const id=req.params.id;
    Student.findByPK(id)
    .then(student=>{
        if (student) {
            return student.destroy();
        }else{
            req.status(404).send("L'étudiant demandé est introuvable");
        }
    })
    .catch(err=>{
        req.status(500)
        .send('Une erreur est survenue lors de la récupération des étudiants');
    });
}